<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form_leads extends Model
{
	protected $table = 'leads_form';
	protected $fillable = ['lead_id','name','email','phone_number','attr_1','attr_2','attr_3','attr_4','attr_5','campaign_id','campaign_channel','channel_id','utm_medium',
	'utm_source','utm_term','utm_campaign','utm_content','ip_address','page_id','page_url','variant','page_name','media','channel','rr'];
    protected $primaryKey = 'lead_id'; 
        public $incrementing = false;

}
