<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Call_leads extends Model
{
	protected $table = 'leads_call';
	protected $fillable = ['id','lead_id','txnRef','session','startTime','endTime','incomingNumber','accessNumber','callType','callState','campaign_id','startTime','endTime','callType','callState','lastInputKey','dest','transferStatus','durationTime','monitorUrl','channel_id','media','channel'];
    protected $primaryKey = 'id'; 
    //public $incrementing = false;
    
}
