<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat_leads extends Model
{
	protected $table = 'leads_chat';
	protected $fillable = ['id','lead_id','name','email','phone_number','attr_1','attr_2','attr_3','attr_4','attr_5','campaign_id','campaign_channel','channel_id','utm_medium','utm_source','utm_term','utm_campaign','utm_content','media','channel','created_at','updated_at'];
    protected $primaryKey = 'id'; 
    //public $incrementing = false;
    
}
