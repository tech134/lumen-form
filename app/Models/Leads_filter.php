<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leads_filter extends Model
{
	protected $table = 'leads_filter';
	protected $fillable = ['id','filter','status'];
    protected $primaryKey = 'id'; 
//public $incrementing = false;
}