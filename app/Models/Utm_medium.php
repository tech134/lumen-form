<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utm_medium extends Model
{
	protected $table = 'utm_medium';
	protected $fillable = ['id','utm_medium','auto','created_at','updated_at'];
    protected $primaryKey = 'id'; 
    //public $incrementing = false;
}