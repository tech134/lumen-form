<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'mailer',
            function ($app) {
                return $app->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
            }
        );
        // Aliases
        $this->app->alias('mailer', \Illuminate\Contracts\Mail\Mailer::class);
        // Make Queue
        $this->app->make('queue');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (isset($_SERVER['LAMBDA_TASK_ROOT'])) {
            // Make sure the directory for compiled views exist
            if (! is_dir(config('view.compiled'))) {
                mkdir(config('view.compiled'), 0755, true);
            }
        }
    }

}
