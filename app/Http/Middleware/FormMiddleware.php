<?php

namespace App\Http\Middleware;

use App\Models\Campaign;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use Closure;

class FormMiddleware
{
    public function handle($request, Closure $next)
    {
                // grab some user
        $request = request();
        $token = $request->bearerToken();
        $this->campaign_id = $request->get('campaign_id');

        $campaign = \DB::table('campaign')->where('campaign_id',$this->campaign_id)->first();

        if (!$campaign) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Campaign does not exist.'
            ], 400);
        }

        foreach ($campaign as $data_campaign){
            $key = $campaign->campaign_key;
        }

        if(!$token) {
            // Unauthorized response if token not there
            return [
                'code' => 500,
                'error' => 'Token not provided.'
            ];
        }

        if (!($key == $token)) {
            return response()->json([
                'token' => "not valid"
            ], 500);
        }
                
        return $next($request);
    }
}
