<?php
namespace App\Http\Controllers;

use App\Models\Form_leads;

use App\Jobs\SendEmailLeadgen;
use App\Jobs\SendEmailChat;
use App\Jobs\SendEmailChat_apps;
use App\Jobs\SendEmailLeadgen_apps;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Redirect,Response;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class FormController extends BaseController
{

    public function store(Request $request)
    {
        $this->media = $request->get('media');

        if ($this->media == "LIVE-CHAT"){
            $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'text' => 'required',
            'campaign_id' => 'required',
            'media' => 'required',
        ]);
        $text = $request->get('text');

        if (strpos($text, 'Phone')) {
        $pattern = '/Phone : /';
        }

        if (strpos($text, 'Telp')) {
        $pattern = '/Telp : /';
        }

        if (strpos($text, 'Telepon')) {
        $pattern = '/Telepon : /';
        }

        list($before, $after) = preg_split($pattern, $text);

        $phone_number = $after;

        }else{
        $phone_number = $request->get('phone_number');
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'campaign_id' => 'required',
            'media' => 'required',
        ]);
        }

        $this->campaign_id = $request->get('campaign_id');
        $this->channel_id = $request->get('channel_id');
        $this->name = $request->get('name');
        $this->email = $request->get('email');

        $utm_medium = $request->get('utm_medium');
        $utm_source = $request->get('utm_source');
        $utm_content = $request->get('utm_content');
        $utm_term = $request->get('utm_term');
        $utm_campaign = $request->get('utm_campaign');

        // $this->time = Carbon::now('Asia/Jakarta'); 
        date_default_timezone_set('Asia/Jakarta');
        $this->time = date('Y-m-d H:i:s');      
        $this->attr_1 = $request->get('attr_1');
        $this->attr_2 = $request->get('attr_2');
        $this->attr_3 = $request->get('attr_3');
        $this->attr_4 = $request->get('attr_4');
        $this->attr_5 = $request->get('attr_5');

        $this->form_id = $request->get('form_id');
        $this->form_name = $request->get('form_name');

        $phone_number = preg_replace('/\s+/', '', $phone_number);;
        $phone_number = str_replace("(","",$phone_number);
        $phone_number = str_replace(")","",$phone_number);
        $phone_number = str_replace(".","",$phone_number);
        $phone_number = str_replace("-","",$phone_number);
        $phone_number = str_replace("+","",$phone_number);   
        if(substr(trim($phone_number), 0, 2)=='62'){
            $phone_number = substr_replace($phone_number,'0',0,2);
        }


        $phone1 = \DB::table('leads_call')->where('incomingNumber',$phone_number)->where('campaign_id',$this->campaign_id )->count();

        $phone2 = \DB::table('leads_chat')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();

        $phone3 = \DB::table('leads_form')->where('phone_number',$phone_number)->where('campaign_id',$this->campaign_id )->count();

        $filter = \DB::table('leads_filter')->where('filter',$phone_number)->count();

        if ($this->media == "LG-FORM" ) {
        $this->junk = \DB::table('leads_form')->where('name',$this->name)->where('phone_number',$phone_number)->where('email',$this->email)->where('attr_1',$this->attr_1)->where('attr_2',$this->attr_2)->where('attr_3',$this->attr_3)->where('attr_4',$this->attr_4)->where('attr_5',$this->attr_5)->where('campaign_id', $this->campaign_id)->count();
        }else if (($this->media == "LP-CHAT") OR ($this->media == "LIVE-CHAT")){
        $this->junk = \DB::table('leads_chat')->where('name',$this->name)->where('phone_number',$phone_number)->where('email',$this->email)->where('attr_1',$this->attr_1)->where('attr_2',$this->attr_2)->where('attr_3',$this->attr_3)->where('attr_4',$this->attr_4)->where('attr_5',$this->attr_5)->where('campaign_id', $this->campaign_id)->count();
        }else{
            return response()->json("media is undefined !", 400);
        }

        $phone = ($phone1 + $phone2 + $phone3);

        if ($this->junk > 0) {
        $junk_message = "You have a junk leads";
          return response()->json($junk_message, 201);
        }elseif ($filter > 0){
        $junk_message = "You have a spam leads";
          return response()->json($junk_message, 201);
        }else{
          
        if ($phone > 0) {
        $status = "Duplicate";
        }elseif (strpos($this->name, 'test') !== false) {
        $status = "Test";
        }elseif (strpos($this->email, 'test') !== false) {
        $status = "Test";
        }else{
        $status = "";
        }
        
        if ($utm_medium == ''){
                $this->channel = "direct";
        }else{
            $auto_placement = \DB::table('utm_medium')->where('utm_medium',$utm_medium)->get();
            foreach ($auto_placement as $data){
                        $auto = $data->auto;
                    }

            if (isset($auto)){
                $this->channel = $auto;
            }else{
                $this->channel = "Undefined";
            }         
        }
        
        if ($this->channel == 'direct' && $utm_source == 'Facebook'){
            $this->channel = "FB";
        }

        if (($this->media == "LP-CHAT") OR ($this->media == "LIVE-CHAT")){

        $posts = \DB::table('leads_chat')->where('lead_id', \DB::raw("(select max(lead_id) from leads_chat)"))->get();
        if (count($posts)){
            foreach ($posts as $data){
            $d = (int)substr($data->lead_id,2,5);
            $d++;
            $char = "CT";
            $lead_id = $char . sprintf("%05s", $d);
        }
        }else{
            $lead_id = "CT00001";
        }

            try {
        \DB::table('leads_chat')->insert([
            'lead_id' => $lead_id,
            'name' => $this->name,
            'email' => $this->email,
            'phone_number' => $phone_number,
            'utm_medium' => $utm_medium,
            'utm_source' => $utm_source,
            'utm_content' => $utm_content,
            'utm_term' => $utm_term,
            'utm_campaign' => $utm_campaign,    
            'attr_1' => $this->attr_1,
            'attr_2' => $this->attr_2,
            'attr_3' => $this->attr_3,
            'attr_4' => $this->attr_4,
            'attr_5' => $this->attr_5,
            'media' => $this->media,
            'channel' => $this->channel,
            'campaign_id' => $this->campaign_id,
            'channel_id' =>  $this->channel_id,
            'created_at' => $this->time
            ]);
                } catch (\Exception $ex) {
                    Log::critical($ex);
                }
        }

        if($this->media == "LG-FORM"){
        $posts = \DB::table('leads_form')->where('lead_id', \DB::raw("(select max(lead_id) from leads_form)"))->get();
        if (count($posts)){
            foreach ($posts as $data){
            $d = (int)substr($data->lead_id,2,5);
            $d++;
            $char = "FM";
            $lead_id = $char . sprintf("%05s", $d);
        }
        }else{
            $lead_id = "FM00001";
        }
            try {
            \DB::table('leads_form')->insert([
            'lead_id' => $lead_id,
            'name' =>  $this->name,
            'email' =>  $this->email,
            'phone_number' => $phone_number,

            'utm_source' => $utm_source,
            'utm_content' => $utm_content,
            'utm_term' => $utm_term,
            'utm_campaign' => $utm_campaign,    
            'utm_medium' => $utm_medium,

            'page_id' =>  $this->form_id,
            'page_name' =>  $this->form_name,

            'attr_1' => $this->attr_1,
            'attr_2' => $this->attr_2,
            'attr_3' => $this->attr_3,
            'attr_4' => $this->attr_4,
            'attr_5' => $this->attr_5,
            'media' => $this->media,
            'channel' => $this->channel,

            'campaign_id' => $this->campaign_id,
            'channel_id' =>  $this->channel_id,
            'created_at' => $this->time
            ]);
                    }catch (\Exception $ex) {
                    Log::critical($ex);
                    }
    }


        $datas = \DB::table('campaign')->where('campaign_id',$this->campaign_id)->first();
          
        $this->herovision = $datas->channel_id;
        $this->campaign_name =  $datas->campaign_name;
        $this->l_attr_1 =  $datas->attr_1;
        $this->l_attr_2 =  $datas->attr_2;
        $this->l_attr_3 =  $datas->attr_3;
        $this->l_attr_4 =  $datas->attr_4;
        $this->l_attr_5 =  $datas->attr_5;

        $to =  $datas->email_client;
        $cc1 =  $datas->cc1;
        $cc2 =  $datas->cc2;
        $cc3 =  $datas->cc3;
        $cc4 =  $datas->cc4;
        $this->crm_url =  $datas->crm_url;
        $this->crm =  $datas->crm;

        $this->accessKey =  $datas->accessKey;
        $this->secretKey =  $datas->secretKey;

        $this->ha_apps =  $datas->ha_apps;
        $this->apps_url =  $datas->apps_url;

        if(isset($this->campaign_name)){
        $campaign =  $this->campaign_name;
        }else{
        $campaign =  "Campaign ID not found";
        }

        $to_mail = [$to, $cc1, $cc2,  $cc3, $cc4];

        //into hero vision
        $data_hp = [
            'time' => $this->time,
            'lead_id' => $lead_id,
            'name' => $this->name,
            'email' => $this->email,
            'phone_number' => $phone_number,
            "attr_1" => empty($this->attr_1) ? "" : $this->attr_1,
            "attr_2" => empty($this->attr_2) ? "" : $this->attr_2,
            "attr_3" => empty($this->attr_3) ? "" : $this->attr_3,
            "attr_4" => empty($this->attr_4) ? "" : $this->attr_4,
            "attr_5" => empty($this->attr_5) ? "" : $this->attr_5,
            'utm_medium' => $utm_medium,
            'utm_content' => $utm_content,
            'utm_source' => $utm_source,
            'utm_term' => $utm_term,
            'utm_campaign' => $utm_campaign,
            'channel' => $this->channel,
            'media' =>  $this->media,
            "page_name" => empty($this->form_id) ? "" : $this->form_id,
            "channel_id" => empty($this->channel_id) ? "" : $this->channel_id ,
        ];

        $chat_msg = "You have new leads from chat -";
        $this->subject_chat =  $chat_msg.' '.$this->campaign_name;

        $leadgen_msg = "You have new leads from leadgen form -";
        $this->subject_leadgen =  $chat_msg.' '.$this->campaign_name;

                //into heroagent
        $data = [
            'time' => $this->time,
            'lead_id' => $lead_id,
            'name' => $this->name,
            'email' => $this->email,
            'phone_number' => $phone_number,
            "attr_1" => empty($this->attr_1) ? "" : $this->attr_1,
            "attr_2" => empty($this->attr_2) ? "" : $this->attr_2,
            "attr_3" => empty($this->attr_3) ? "" : $this->attr_3,
            "attr_4" => empty($this->attr_4) ? "" : $this->attr_4,
            "attr_5" => empty($this->attr_5) ? "" : $this->attr_5,
            'utm_medium' => $utm_medium,
            'utm_content' => $utm_content,
            'utm_source' => $utm_source,
            'utm_term' => $utm_term,
            'utm_campaign' => $utm_campaign,
            'campaign_name' => $this->campaign_name,
            'l_attr_1' => $this->l_attr_1,
            'l_attr_2' => $this->l_attr_2,
            'l_attr_3' => $this->l_attr_3,
            'l_attr_4' => $this->l_attr_4,
            'l_attr_5' => $this->l_attr_5,
            'channel' => $this->channel,
            'media' =>  $this->media,
            'page_id' => empty($this->form_id) ? "" : $this->form_id,
            'page_name' => empty($this->form_name) ? "" : $this->form_name,
            'status' => $status,
            'to' => $to_mail,
            'campaign_id' => $this->campaign_id,
            'subject_chat' => $this->subject_chat,
            'subject_leadgen' => $this->subject_leadgen,
            'apps_url' => $this->apps_url,
        ];


        if (($this->herovision == "active") AND (($this->media == "LP-CHAT") OR ($this->media == "LIVE-CHAT")) AND isset($this->channel_id)) {
            try {
        //for pruductions https://leadservice.heroleads.co.th/LandingPageCallService?token=thioch4eimovoiDu6ahd&system=chatbot
            $client = new Client(
                    [
                        'base_uri' => 'https://leadservice.heroleads.co.th/',
                        'timeout'  => 5,
                        'headers' => [
                            "Content-Type" => 'application/json'
                        ]
                    ]
                );
                $json_response =  $client->request('POST','LandingPageCallService?token=thioch4eimovoiDu6ahd&system=chatbot',['json' => $data_hp]);
                Log::info(json_encode($json_response->getBody()));
            } catch (\Exception $ex) {
                Log::critical($ex);
            }
        }
        if (($this->herovision == "active") AND ($this->media == "LG-FORM") AND isset($this->channel_id)) {
                try {
                //for production url is https://leadservice.heroleads.co.th/LandingPageCallService?token=thioch4eimovoiDu6ahd
                $client = new Client(
                    [
                        'base_uri' => 'https://leadservice.heroleads.co.th/',
                        'timeout'  => 5,
                        'headers' => [
                            "Content-Type" => 'application/json'
                        ]
                    ]
                );
                $json_response =  $client->request('POST','LandingPageCallService?token=thioch4eimovoiDu6ahd',['json' => $data_hp]);
                    Log::info(json_encode($json_response->getBody()));
                } catch (\Exception $ex) {
                    Log::critical($ex);
                }
        }

        if ($this->crm == "Google Sheets") {
            try {
                $client = new Client(
                    [
                        'timeout'  => 5,
                        'headers' => [
                            "Content-Type" => 'application/json'
                        ]
                    ]
                );
                $json_response =  $client->request('POST', $this->crm_url,['json' => $data]);
                Log::info(json_encode($json_response->getBody()));
            } catch (\Exception $ex) {
                Log::critical($ex);
            }
        }            

        if($status == "Test"){
        }else{
            if(isset($to)){
                if (($this->media  == "LP-CHAT") OR ($this->media  == "LIVE-CHAT")){
                    if($this->ha_apps == "active"){
                        Queue::later(Carbon::now()->addMinutes(3),new SendEmailChat_apps($data));
                    }else{
                        Queue::later(Carbon::now()->addMinutes(1),new SendEmailChat($data));
                    }
                    // Mail::send('mail.chat_notif_1' ,$data , function($message) use ($to,$cc1,$cc2,$cc3,$cc4) {
                    // $message->to($to)->cc($cc1)->cc($cc2)->cc($cc3)->cc($cc4)->subject($this->subject_chat);
                    //  $message->from('NoReply@heroleads.asia','HEROLEADS');
                    // });               
                }else 
                if ($this->media  == "LG-FORM"){
                    if($this->ha_apps == "active"){
                        Queue::later(Carbon::now()->addMinutes(3),new SendEmailLeadgen_apps($data));
                    }else{
                        Queue::later(Carbon::now()->addMinutes(1),new SendEmailLeadgen($data));
                    }
                    // Mail::send('mail.leadgen_1' ,$data , function($message) use ($to,$cc1,$cc2,$cc3,$cc4) {
                    // $message->to($to)->cc($cc1)->cc($cc2)->cc($cc3)->cc($cc4)->subject($this->subject_leadgen);
                    //  $message->from('NoReply@heroleads.asia','HEROLEADS');
                    // });
                }    
            }
        }

            $success = "Success";
            return response()->json($success, 201);
        }
    }
}