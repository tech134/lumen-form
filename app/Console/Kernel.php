<?php

namespace App\Console;


// use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('queue:listen')->everyTwoMinutes();

    //     $schedule->call(function () {
            
    //     //Pengecekan apakah cronjob berhasil atau tidak
    // //Mencatat info log 
    //         Log::Critical('Cronjob berhasil dijalankan');
    //     })->everyTwoMinutes();
    }
}
