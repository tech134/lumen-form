<!DOCTYPE html>
<html>
  <body style="font-family: Open Sans, sans-serif; font-size: 16px; width: auto; background: #f7f7f7; padding: 10px;">
    <div style="background: white; max-width: 500px; border-top: solid 3px #ff3e00; box-shadow: 0 0 25px rgba(0,0,0,0.05); margin:auto;">
      <div class="header" style=" padding: 30px; background: #fafafa; border-bottom: solid 1px #f2f2f2"> 
        <img src="https://heroleads.id/assets/heroleads-logo-colored.png" style="display: block; margin: auto; width: 200px"></div>
      <div class="content" style="padding: 30px; padding-bottom: 40px;">
        <p style="text-align: center; margin:0;">Hi Partner,</p>
        <h2 style="font-size: 24px; text-align: center; margin-botom:20px; mso-line-height-rule:exactly;">You have new leads from campaign: {{ $data['campaign_name'] }} </h2>
        <table style="width: 400px; color: #999; font-size: 13px; margin: auto;">
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Date</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['time'] }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Name</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['name'] }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Email</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['email'] }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Phone Number</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['phone_number'] }}</td>
          </tr>
          @if (empty($data['attr_1'])) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $data['l_attr_1'] }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['attr_1'] }}</td>
          </tr>
          @endif
          @if (empty($data['attr_2'])) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $data['l_attr_2'] }}</td><td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['$attr_2'] }}</td>
          </tr>
          @endif
          @if (empty($data['attr_3'])) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $data['l_attr_3'] }}</td><td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['attr_3'] }}</td>
          </tr>
          @endif
          @if (empty($data['attr_4']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $data['l_attr_4'] }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['attr_4'] }}</td>
          </tr>
          @endif
          @if (empty($data['attr_5']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $data['l_attr_5'] }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['attr_5'] }}</td>
          </tr>
          @endif
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Media</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['media'] }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Channel</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['channel'] }}</td>
          </tr>
          @if (empty($data['utm_content']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Utm Content</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['utm_content'] }}</td>
          </tr>
          @endif
          @if (empty($data['utm_medium']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Utm Medium</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['utm_medium'] }}</td>
          </tr>
          @endif
          @if (empty($data['utm_campaign']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Utm Campaign</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['utm_campaign'] }}</td>
          </tr>
          @endif
          @if (empty($data['utm_term']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Utm Term</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['utm_term'] }}</td>
          </tr>
          @endif
          @if (empty($data['utm_source']))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Utm Source</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $data['utm_source'] }}</td>
          </tr>
          @endif
        </table>
        <br>
        @if (empty($data['apps_url']))
        <table style="margin: auto;"><tr><td>
        @php($url = "")
      <a href="{{ $url }}" style="padding-left: 40px; padding-right: 40px; padding-top: 15px; padding-bottom: 15px; background: #ff3e00; color: white; text-decoration: none; text-align: center; border-radius: 100px; border: 1px solid #ff3e00; font-weight:700; display: block;">Follow Up Now</a></td></tr>
      </table>
        @else        
      <table style="margin: auto;"><tr><td>
    <a href="{{ $data['apps_url'] }}" style="padding-left: 40px; padding-right: 40px; padding-top: 15px; padding-bottom: 15px; background: #ff3e00; color: white; text-decoration: none; text-align: center; border-radius: 100px; border: 1px solid #ff3e00; font-weight:700; display: block;">Follow Up Now</a></td></tr>
      </table>
        @endif
      </div>
      <div class="footer" style="background: black; padding: 30px; color: grey;line-height: 18px; font-size: 12px; text-align: center;">Sampoerna Stategic Square North Tower Lt. 25 - GoWork<br>Jl. Jend. Sudirman Kav. 45, Karet Semanggi,<br>Setiabudi Jakarta Selatan 12930<br>info.id@heroleads.asia | 6221 300 30 700</div>
    </div>
  </body>
</html> 