<html>
  <body style="font-family: Open Sans, sans-serif; font-size: 16px; width: auto; background: #f7f7f7; padding: 10px;">
    <div style="background: white; max-width: 500px; border-top: solid 3px #ff3e00; box-shadow: 0 0 25px rgba(0,0,0,0.05); margin:auto;">
      <div class="header" style=" padding: 30px; background: #fafafa; border-bottom: solid 1px #f2f2f2"> 
        <img src="https://heroleads.id/assets/heroleads-logo-colored.png" style="display: block; margin: auto; width: 200px"></div>
      <div class="content" style="padding: 30px; padding-bottom: 40px;"><p style="text-align: center; margin:0;">Hi Partner,</p>
        <h2 style="font-size: 24px; text-align: center; margin-botom:20px; mso-line-height-rule:exactly;">You have new leads from campaign: {{ $campaign_name }}</h2>
        <table style="width: 400px; color: #999; font-size: 13px; margin: auto;">
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Date</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $time }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Name</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $name }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Email</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $email }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Phone Number</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $phone_number }}</td>
          </tr>
          @if (empty($attr_1)) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $l_attr_1 }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $attr_1 }}</td>
          </tr>
          @endif
          @if (empty($attr_2)) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $l_attr_2 }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $attr_2 }}</td>
          </tr>
          @endif
          @if (empty($attr_3)) 
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $l_attr_3 }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $attr_3 }}</td>
          </tr>
          @endif
          @if (empty($attr_4))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $l_attr_4 }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $attr_4 }}</td>
          </tr>
          @endif
          @if (empty($attr_5))
          @else
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">{{ $l_attr_5 }}</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $attr_5 }}</td>
          </tr>
          @endif
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Form ID</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $page_id }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Media</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td>
            <td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $media }}</td>
          </tr>
          <tr>
            <td class="col1" style="padding: 15px; border-bottom: 1px solid #f7f7f7;color: black;">Channel</td>
            <td style="width: 2px; border-bottom: 1px solid #f7f7f7;">:</td><td style="padding: 15px; border-bottom: 1px solid #f7f7f7;">{{ $channel }}</td>
          </tr>
        </table>
      </div>
      <div class="footer" style="background: black; padding: 30px; color: grey;line-height: 18px; font-size: 12px; text-align: center;">Sampoerna Stategic Square North Tower Lt. 25 - GoWork<br>Jl. Jend. Sudirman Kav. 45, Karet Semanggi,<br>Setiabudi Jakarta Selatan 12930<br>info.id@heroleads.asia | 6221 300 30 700</div>
    </div>
  </body>
</html>