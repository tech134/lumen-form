<?php
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Jobs\SendEmailChat;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
 
// $router->get('/login', function (Request $request) {
//     $token = app('auth')->attempt($request->only('email', 'password'));
 
//     return response()->json(compact('token'));
// });

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// $router->get('test', function () use ($router) {
//     $details['email'] = 'kzyahrial@gmail.com';
//     dispatch(new SendEmailChat($details));
// });

// $router->post('register', 'AuthController@register');
// $router->post('login', 'AuthController@login');


// $router->get('login', function (Request $request) {
//     $token = app('auth')->attempt($request->only('email', 'password'));
 
//     return response()->json(compact('token'));
// });

// Route::group([
//     'middleware' => 'auth',
//     'prefix' => 'auth'

// ], function ($router) {
//     Route::post('/login', [AuthController::class, 'login']);
//     Route::post('/register', [AuthController::class, 'register']);
//     Route::post('/logout', [AuthController::class, 'logout']);
//     Route::post('/refresh', [AuthController::class, 'refresh']);
//     Route::get('/user-profile', [AuthController::class, 'userProfile']);    
// });

$router->post(
    'auth/login', 
    [
       'uses' => 'AuthController@authenticate'
    ]
);

$router->group(['middleware' => 'form.api'], function () use ($router) {
    $router->post('api/general-form',
    [
       'uses' => 'FormController@store'
    ]
);
});

$router->group(['middleware' => 'form1.api'], function () use ($router) {
    $router->post('api/general-form1',
    [
       'uses' => 'FormController@store'
    ]
);
});
